/*************************************************************************
   LAB 1

    Edit this file ONLY!

*************************************************************************/
#include "dns.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>


unsigned int getHash(char* s);
struct Address* create_address(char* domain, unsigned int ip);
void insert(struct Address* hashtable[], char* domain, unsigned int ip);
void delete(struct Address* hashtable[]);
unsigned int search(struct Address* hashtable[], char* domain);

IPADDRESS createIp(int ip1, int ip2, int ip3, int ip4) {
    return (ip1 & 0xFF) << 24 |
        (ip2 & 0xFF) << 16 |
        (ip3 & 0xFF) << 8 |
        (ip4 & 0xFF);
}
#define MAXCHAR 256
#define HashtableSize 10000

struct Address
{
    struct Address* next;
    char* domain;
    unsigned int ip;
};

struct Address* hashtable[HashtableSize];

unsigned int getHash(char* s) {
    unsigned hashval;
    for (hashval = 0; *s != '\0'; s++)
        hashval = *s + 31 * hashval;//32
    return hashval % HashtableSize;
}

struct Address* create_address(char* domain, unsigned int ip) {
    struct Address* result = malloc(sizeof(struct Address));
    result->domain = malloc(strlen(domain) + 1);
    strcpy(result->domain, domain);
    result->ip = ip;
    result->next = NULL;
    return result;
}

void insert(struct Address* hashtable[], char* domain, unsigned int ip) {
    unsigned int index = getHash(domain);

    struct Address* new_Address = create_address(domain, ip);

    if (hashtable[index] == NULL) {
        hashtable[index] = new_Address;
    }
    else {
        struct Address* current = hashtable[index];
        while (current->next != NULL) {
            current = current->next;
        }
        current->next = new_Address;
    }
}

void delete(struct Address* hashtable[]) {
    for (int i = 0; i < HashtableSize; i++)
    {
        struct Address* current = hashtable[i];
        if (current != NULL) {
            struct Address* next = current->next;
            while (current != NULL) {
            	free(current->domain);
                free(current);
                current = next;
                if (next != NULL)
                    next = next->next;
            }
        }
    }
}

unsigned int search(struct Address* hashtable[], char* domain) {
    unsigned int index = getHash(domain);
    struct Address* current = hashtable[index];
    if (current == NULL)
        return INVALID_IP_ADDRESS;
    while (current != NULL) {
        if (strcmp(domain, current->domain) == 0)
            return current->ip;
        current = current->next;
    }
    return INVALID_IP_ADDRESS;
}

DNSHandle InitDNS()
{
    return (DNSHandle)1;
}

void LoadHostsFile(DNSHandle hDNS, const char* hostsFilePath)
{
    FILE* fp;
    fp = fopen(hostsFilePath, "r");
    char str[MAXCHAR];
    short count = 0;
    unsigned int ip1 = 0, ip2 = 0, ip3 = 0, ip4 = 0;
    while (fgets(str, MAXCHAR, fp)) {
        fscanf_s(fp, "%d.%d.%d.%d %s", &ip1, &ip2, &ip3, &ip4, str, 200);
        IPADDRESS ip = createIp(ip1, ip2, ip3, ip4);
        insert(hashtable, str, ip);
    }
    

}

IPADDRESS DnsLookUp(DNSHandle hDNS, const char* hostName)
{
    return search(hashtable, hostName);
}

void ShutdownDNS(DNSHandle hDNS)
{
    delete(hashtable);
}
